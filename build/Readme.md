# Compiling openqcd-hadspec

The openqcd-hadspec build process is more or less the same as the one of
openqcd-fastsum (and all the other derivatives). Configuration of the build is
handled using the `compile-settings.txt` file, and the build is carried out in a
relative manner, making it easy to keep multiple versions of the executables and
libraries.

## Prerequisites

The code depends on openQCD-FASTSUM v1.1 (which includes the library feature) as
well as openqcd-propagator v1.0.  The header and library files produced by these
two programs must be available at compilation, and one can specify its path to
the compiler through setting the `CFLAGS` and `LDFLAGS` of the configuration
(see next section), or by setting the `OPENQCDLOC` and optionally also
`OPENQCDPROPLOC` flags if they are installed in separate directories if they are
installed in separate directories. The code assumes that these two have been
compiled as a C++ library.

To compile the tests one also has to have a C++14 compatible MPI compiler
available.

## Options

The compilation has the following options:

```
 * CODELOC (required):
     Path to the root directory of the code, can be relative or absolute
 * OPENQCDLOC (optional):
     Root location of the openqcd-fastsum library installation
 * OPENQCDPROPLOC (optional):
     Root location of the openqcd-propagator library installation, only
     necessary if this directory is different from OPENQCDLOC
 * COMPILER.CC (optional, default: mpicc):
     Command to use to compile C-code
 * COMPILER.CXX (optional, default: mpicxx):
     Command to use to compile C++ code, the unit tests
 * MPI_INCLUDE (optional):
     Directory where the MPI header files are located
 * CFLAGS (optional):
     Additional C-flags to compile with
 * LDFLAGS (optional):
     Additional linker flags to compile with
```

An example compile_settings file is included in the repository. 

## Build targets

The makefile have multiple targets depending on what needs to be compiled:

 * `all`: (default) compiles the `openqcd-hadspec` executable
 * `tests`: compiles the unit tests
 * `install`: compile library and executables and move them to `$PREFIX`

In the case of `install` everything is compiled, the binary is copied to
`$PREFIX/bin`. Setting `PREFIX` can for instance be done by calling `make` as:

```bash
make install PREFIX=$HOME/usr
```

If `PREFIX` isn't set it defaults to `.`, the current directory.
