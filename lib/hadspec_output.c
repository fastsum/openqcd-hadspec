/*******************************************************************************
 *
 * File hadspec_output.c
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * The externally accessible functions are
 *
 *   void print_doublet(char const *filename, complex_dble const *doublet,
 *                      int isrc)
 *     Print the data from doublet to a file named filename. In the process it
 *     also collects terms on other MPI processes and collects them into an N0
 *     long array.
 *
 *   void print_quadruplet(char const *filename, complex_dble const *quadruplet,
 *                         int isrc)
 *     Print the data from quadruplet to a file named filename.
 *
 *   void print_meson(char const *filename, complex_dble const *meson, int isrc)
 *     Print the data from quadruplet to a file named filename.
 *
 * Notes:
 *
 *   All routines takes the local correlators of length L0 and concats it into
 *   one long correlator of length L0*NPROC0. In the process the time-series is
 *   shifted so that the time-slice the source lives on is time = 0. In the case
 *   of the baryons, which are fermionic particles, they have to be multiplied
 *   by -1 as one crosses Nt. The mesonic correlators are stored in a similar
 *   way to the mu^2, namely that there are 16 rows per time-slice, one for each
 *   gamma matrix.
 *
 *******************************************************************************/

#define HADSPEC_OUTPUT_C
#define OPENQCD_INTERNAL

#ifdef __cplusplus
extern "C" {
#endif

#include "hadspec.h"

#include "openqcd/c_headers/flags.h"
#include "openqcd/c_headers/global.h"
#include "openqcd/c_headers/utils.h"

#include "openqcd-propagator/c_headers/sources.h"

#ifdef __cplusplus
}
#endif

#define N0 (NPROC0 * L0)

#include <mpi.h>

/* Order an array in time starting from time t_src
 * Output only given to rank 0 */
static complex_dble *time_order_array(int t_src, complex_dble const *input,
                                      int stride, int fermion)
{
  complex_dble *output, *rbuf;
  int my_rank, mpi_next;
  int it, ival, modit, full_it, tag;
  int sign;
  MPI_Status stat;

  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  tag = mpi_tag();

  if (my_rank == 0) {
    output = (complex_dble *)amalloc(stride * N0 * sizeof(*output), ALIGN);

    error_root(output == NULL, 1, "print_doublet [hadspec_baryons.c]",
               "Unable to allocate output buffer array");

    /* Fill the local contribution */
    for (it = 0, full_it = 0; it < L0; ++it, ++full_it) {

      /* Might have to flip sign depending on whether we cross the temporal
       * direction or not, and whether the particle is a fermion or a boson */
      sign = (1 - 2 * (fermion != 0) * (full_it < t_src));
      modit = (N0 + full_it - t_src) % N0;

      for (ival = 0; ival < stride; ++ival) {
        output[ival + stride * modit].re = sign * input[ival + stride * it].re;
        output[ival + stride * modit].im = sign * input[ival + stride * it].im;
      }
    }

    /* If there are more processes in t-dir we need to fetch these */
    if (NPROC0 > 1) {
      rbuf = (complex_dble *)amalloc(stride * L0 * sizeof(*rbuf), ALIGN);

      error_root(rbuf == NULL, 1,
                 "store_one_to_all_results [openqcd-mu-squared.c]",
                 "Unable to allocate communication buffer array");

      mpi_next = npr[1];

      while (mpi_next != 0) {
        /* Fetch the data */
        MPI_Recv(rbuf, 2 * stride * L0, MPI_DOUBLE, mpi_next, tag,
                 MPI_COMM_WORLD, &stat);

        /* Fetch the processor id of the next neighbour */
        MPI_Recv(&mpi_next, 1, MPI_INT, mpi_next, tag, MPI_COMM_WORLD, &stat);

        /* Place data appropriately in the savebuf matrix */
        for (it = 0; it < L0; ++it, ++full_it) {
          /* Boundary condition sign flip */
          sign = (1 - 2 * (fermion != 0) * (full_it < t_src));
          modit = (N0 + full_it - t_src) % N0;

          for (ival = 0; ival < stride; ++ival) {
            output[ival + stride * modit].re =
                sign * rbuf[ival + stride * it].re;
            output[ival + stride * modit].im =
                sign * rbuf[ival + stride * it].im;
          }
        }
      }

      /* Free the MPI communication buffer memory */
      afree(rbuf);
    }

  } else if ((cpr[1] + cpr[2] + cpr[3]) == 0) {
    /* If you are a {nx, ny, nz} = 0 node, you need to send your data to
     * processor 0 */
    MPI_Send(input, 2 * stride * L0, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD);
    MPI_Send(npr + 1, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
  }

  return output;
}

/* Print an array stored in data expected to hold stride*L0 elements into a
 * file. The stride tells us how many entries the array has per time slice, e.g.
 * a meson has 16, one for each gamma matrix. If the particle is a fermion we
 * multiply by -1 as we cross t=Nt. */
static void print_array(char const *filename, complex_dble const *data,
                        int stride, int isrc, int fermion)
{
  FILE *fop;
  complex_dble *savebuf;
  int it, ival;
  int my_rank;
  source_parms_t src;

  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  src = source_parms(isrc);
  savebuf = time_order_array(src.pos[0], data, stride, fermion);

  if (my_rank == 0) {
    /* Open the output file for writing */
    fop = fopen(filename, "w");

    /* Write the reordered data to file */
    for (it = 0; it < N0; ++it) {
      for (ival = 0; ival < stride; ++ival) {
        fprintf(fop, "%.12e %.12e ", savebuf[ival + stride * it].re,
                savebuf[ival + stride * it].im);
      }
      fprintf(fop, "\n");
    }

    fclose(fop);

    /* Free the reordering matrix buffer memory */
    afree(savebuf);
  }
}

void print_doublet(char const *filename, complex_dble const *doublet, int isrc)
{
  print_array(filename, doublet, 1, isrc, 1);
}

void print_quadruplet(char const *filename, complex_dble const *quadruplet,
                      int isrc)
{
  print_array(filename, quadruplet, 1, isrc, 1);
}

void print_meson(char const *filename, complex_dble const *meson, int isrc)
{
  print_array(filename, meson, 16, isrc, 0);
}
