/*******************************************************************************
 *
 * File hadspec_baryons.c
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * The externally accessible functions are
 *
 *   size_t doublet_size(void)
 *     Returns the size of the buffer necessary to store the result for
 *     computing the doublet (J=1/2) baryons.
 *
 *   size_t quadruplet_size(void)
 *     Returns the size of the buffer nexessary to store the result for
 *     computing the quadruplet (J=3/2) baryons.
 *
 *   void doublet_2fl_baryon(int iquark1, int iquark2, complex_dble *out)
 *     Compute the 2 flavour baryon doublet state using the quarks specificed by
 *     iquark1 and iquark2, the result is stored in out which is expected to
 *     be of size doublet_size.
 *
 *   void quadruplet_1fl_baryon(int iquark, complex_dble *out)
 *     Compute the 1 flavour baryon quadruplet state using quark iquark and
 *     store the result in out.
 *
 *   void quadruplet_2fl_baryon(int iquark1, int iquark2, complex_dble *out)
 *     Compute the 2 flavour baryon quadruplet state using the quarks specificed
 *     by iquark1 and iquark2, the result is stored in out which is expected to
 *     be of size quadruplet_size.
 *
 * Notes:
 *
 *   All computation algorithms must communicate and therefore have to be called
 *   simultaneously on all processes. The routines also expect that the
 *   propagators for the quarks have been computed beforehand and is accessible
 *   through the quark_propagator interface.
 *
 *******************************************************************************/

#define HADSPEC_BARYONS_C
#define OPENQCD_INTERNAL

#ifdef __cplusplus
extern "C" {
#endif

#include "hadspec.h"
#include "spin_matrix.h"

#include "openqcd/c_headers/global.h"
#include "openqcd/c_headers/linalg.h"
#include "openqcd/c_headers/sflds.h"
#include "openqcd/c_headers/utils.h"

#ifdef __cplusplus
}
#endif

#include <mpi.h>
static MPI_Comm *spatial_mpi_communicator = NULL;

static const int num_doub = 1;
static const int num_quad = 1;

/* Combination of indices of the Levi-Civita symbol, positive ones first then
 * the negative ones. Basically levi_civita[n] gives a list of colour-indices
 * one has to choose for a non-zero result. There are 6 possible permutations, 3
 * positive and 3 negative. */
static const int levi_civita[6][3] = {{0, 1, 2}, {1, 2, 0}, {2, 0, 1},
                                      {0, 2, 1}, {2, 1, 0}, {1, 0, 2}};

/* Make MPI communication groups so that we can do sums over space but not time
 * accross processes */
static void allocate_spatial_MPI_group(int my_rank)
{
  spatial_mpi_communicator =
      (MPI_Comm *)malloc(sizeof(*spatial_mpi_communicator));
  MPI_Comm_split(MPI_COMM_WORLD, cpr[0], my_rank, spatial_mpi_communicator);
}

/* Compute the terms (stored in result)
 *  1. A tr (G_ii B G_ij C)
 *  2. A G_ii B G_ij C
 * which are the principle components of the baryon correlation functions. In
 * this case G_mu = C gamma_mu, where C is the charge conjugation operator. */
static void unprojected_baryon_component(spin_matrix const *smA,
                                         spin_matrix const *smB,
                                         spin_matrix const *smC, int ii, int ij,
                                         spin_matrix *result)
{
  spin_matrix tmp;

  tmp = transpose_spin_matrix(smC);
  tmp = mul_charge_gamma(&tmp, ij);
  tmp = mul_spin_spin_matrix(&tmp, smB);
  tmp = mul_charge_gamma(&tmp, ii);

  result[0] = mulc_spin_matrix(trace_spin_matrix(&tmp), smA);
  result[1] = mul_spin_spin_matrixransposed_matrix(smA, &tmp);
}

/* Size of the local result buffer for the doublet (J=1/2) states */
size_t doublet_size(void)
{
  return sizeof(complex_dble) * num_doub * L0;
}

/* Size of the local result buffer for the quatruplet (J=3/2) states */
size_t quadruplet_size(void)
{
  return sizeof(complex_dble) * num_quad * L0;
}

/* Compute the 2 flavour J=1/2 baryon correlator */
void doublet_2fl_baryon(int iquark1, int iquark2, complex_dble *out)
{
  int ix, it, ia, ib;
  int sign, my_rank;
  prop_matrix pA, pB;
  spin_matrix smA, smB, smC, tmp[2];
  complex_dble tmp_tr[2];
  complex_dble *sum_buffer;
  spinor_dble **prop1, **prop2;

  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

  if (spatial_mpi_communicator == NULL) {
    allocate_spatial_MPI_group(my_rank);
  }

  prop1 = quark_propagators(iquark1);
  prop2 = quark_propagators(iquark2);

  sum_buffer = (complex_dble *)amalloc(2 * L0 * sizeof(*sum_buffer), ALIGN);

  for (it = 0; it < num_doub * L0; ++it) {
    sum_buffer[it].re = 0.0;
    sum_buffer[it].im = 0.0;
  }

  /* Loop over the lattice volume */
  for (ix = 0; ix < VOLUME; ++ix) {

    it = ix / (L1 * L2 * L3);

    pA = fetch_prop_matrix(prop1, ipt[ix]);
    pB = fetch_prop_matrix(prop2, ipt[ix]);

    /* Loop over colour indice combinations */
    for (ia = 0; ia < 6; ++ia) {
      for (ib = 0; ib < 6; ++ib) {
        sign = (1 - 2 * (ia > 2)) * (1 - 2 * (ib > 2));

        smA = fetch_spin_matrix(&pA, levi_civita[ia][0], levi_civita[ib][0]);
        smB = fetch_spin_matrix(&pB, levi_civita[ia][1], levi_civita[ib][1]);
        smC = fetch_spin_matrix(&pA, levi_civita[ia][2], levi_civita[ib][2]);

        unprojected_baryon_component(&smA, &smB, &smC, 4, 4, tmp);

        /* Compute the 2-flavour J=1/2 state */
        tmp_tr[0] = mul_parity_project_trace(&(tmp[0]), 0);
        tmp_tr[1] = mul_parity_project_trace(&(tmp[1]), 0);

        sum_buffer[num_doub * it + 0].re +=
            sign * (tmp_tr[0].re + tmp_tr[1].re);
        sum_buffer[num_doub * it + 0].im +=
            sign * (tmp_tr[0].im + tmp_tr[1].im);
      }
    }
  }

  /* Sum over the spatial subprocesses */
  MPI_Allreduce(sum_buffer, out, 2 * num_doub * L0, MPI_DOUBLE, MPI_SUM,
                *spatial_mpi_communicator);

  afree(sum_buffer);
}

/* Compute the 1 flavour J=3/2 baryon */
void quadruplet_1fl_baryon(int iquark, complex_dble *out)
{
  int ix, it, ia, ib, inu;
  int sign, my_rank;
  prop_matrix pA;
  spin_matrix smA, smB, smC, tmp[2];
  complex_dble tmp_tr[2];
  complex_dble *sum_buffer;
  spinor_dble **prop;

  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

  if (spatial_mpi_communicator == NULL) {
    allocate_spatial_MPI_group(my_rank);
  }

  prop = quark_propagators(iquark);

  sum_buffer =
      (complex_dble *)amalloc(num_quad * L0 * sizeof(*sum_buffer), ALIGN);

  for (it = 0; it < num_quad * L0; ++it) {
    sum_buffer[it].re = 0.0;
    sum_buffer[it].im = 0.0;
  }

  /* Loop over the lattice volume */
  for (ix = 0; ix < VOLUME; ++ix) {

    it = ix / (L1 * L2 * L3);

    pA = fetch_prop_matrix(prop, ipt[ix]);

    /* Loop over colour indice combinations */
    for (ia = 0; ia < 6; ++ia) {
      for (ib = 0; ib < 6; ++ib) {

        smA = fetch_spin_matrix(&pA, levi_civita[ia][0], levi_civita[ib][0]);
        smB = fetch_spin_matrix(&pA, levi_civita[ia][1], levi_civita[ib][1]);
        smC = fetch_spin_matrix(&pA, levi_civita[ia][2], levi_civita[ib][2]);

        /* Loop over Lorentz indices for the projection */
        sign = (1 - 2 * (ia > 2)) * (1 - 2 * (ib > 2));
        for (inu = 0; inu < 4; ++inu) {
          unprojected_baryon_component(&smA, &smB, &smC, 3, inu, tmp);
          tmp_tr[0] = mul_spin_parity_project_trace[inu][3](&(tmp[0]));
          tmp_tr[1] = mul_spin_parity_project_trace[inu][3](&(tmp[1]));

          sum_buffer[num_quad * it + 0].re +=
              sign * (2 * tmp_tr[0].re + 4 * tmp_tr[1].re);
          sum_buffer[num_quad * it + 0].im +=
              sign * (2 * tmp_tr[0].im + 4 * tmp_tr[1].im);
        }
      }
    }
  }

  /* Sum over the spatial subprocesses */
  MPI_Allreduce(sum_buffer, out, 2 * num_quad * L0, MPI_DOUBLE, MPI_SUM,
                *spatial_mpi_communicator);

  afree(sum_buffer);
}

/* Compute the 2 flavour J=3/2 baryon */
void quadruplet_2fl_baryon(int iquark1, int iquark2, complex_dble *out)
{
  int ix, it, ia, ib, inu;
  int sign, my_rank;
  prop_matrix pA, pB;
  spin_matrix smA, smB, smC, tmp[2];
  complex_dble tmp_tr[2];
  complex_dble *sum_buffer;
  spinor_dble **prop1, **prop2;

  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

  if (spatial_mpi_communicator == NULL) {
    allocate_spatial_MPI_group(my_rank);
  }

  prop1 = quark_propagators(iquark1);
  prop2 = quark_propagators(iquark2);

  sum_buffer =
      (complex_dble *)amalloc(num_quad * L0 * sizeof(*sum_buffer), ALIGN);

  for (it = 0; it < num_quad * L0; ++it) {
    sum_buffer[it].re = 0.0;
    sum_buffer[it].im = 0.0;
  }

  /* Loop over the lattice volume */
  for (ix = 0; ix < VOLUME; ++ix) {

    it = ix / (L1 * L2 * L3);

    pA = fetch_prop_matrix(prop1, ipt[ix]);
    pB = fetch_prop_matrix(prop2, ipt[ix]);

    /* Loop over colour indice combinations */
    for (ia = 0; ia < 6; ++ia) {
      for (ib = 0; ib < 6; ++ib) {

        smA = fetch_spin_matrix(&pA, levi_civita[ia][0], levi_civita[ib][0]);
        smB = fetch_spin_matrix(&pA, levi_civita[ia][1], levi_civita[ib][1]);
        smC = fetch_spin_matrix(&pB, levi_civita[ia][2], levi_civita[ib][2]);

        /* Loop over Lorentz indices for the projection */
        sign = (1 - 2 * (ia > 2)) * (1 - 2 * (ib > 2));
        for (inu = 0; inu < 4; ++inu) {

          /* There are 3 different combinations that contribute */
          /* P1 G_mu P1 G_nu P2 */
          unprojected_baryon_component(&smA, &smB, &smC, 3, inu, tmp);
          tmp_tr[1] = mul_spin_parity_project_trace[inu][3](&(tmp[1]));

          sum_buffer[num_quad * it + 0].re += sign * 4 * tmp_tr[1].re;
          sum_buffer[num_quad * it + 0].im += sign * 4 * tmp_tr[1].im;

          /* P1 G_mu P2 G_nu P1 */
          unprojected_baryon_component(&smA, &smC, &smB, 3, inu, tmp);
          tmp_tr[0] = mul_spin_parity_project_trace[inu][3](&(tmp[0]));
          tmp_tr[1] = mul_spin_parity_project_trace[inu][3](&(tmp[1]));

          sum_buffer[num_quad * it + 0].re +=
              sign * 4 * (tmp_tr[0].re + tmp_tr[1].re);
          sum_buffer[num_quad * it + 0].im +=
              sign * 4 * (tmp_tr[0].im + tmp_tr[1].im);

          /* P2 G_mu P1 G_nu P1 */
          unprojected_baryon_component(&smC, &smA, &smB, 3, inu, tmp);
          tmp_tr[0] = mul_spin_parity_project_trace[inu][3](&(tmp[0]));
          tmp_tr[1] = mul_spin_parity_project_trace[inu][3](&(tmp[1]));

          sum_buffer[num_quad * it + 0].re +=
              sign * (2 * tmp_tr[0].re + 4 * tmp_tr[1].re);
          sum_buffer[num_quad * it + 0].im +=
              sign * (2 * tmp_tr[0].im + 4 * tmp_tr[1].im);
        }
      }
    }
  }

  /* Sum over the spatial subprocesses */
  MPI_Allreduce(sum_buffer, out, 2 * num_quad * L0, MPI_DOUBLE, MPI_SUM,
                *spatial_mpi_communicator);

  afree(sum_buffer);
}
