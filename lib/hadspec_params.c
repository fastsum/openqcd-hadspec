/*******************************************************************************
 *
 * File hadspec_parms.c
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * The externally accessible functions are
 *
 *   void reset_quark_parms(void)
 *     Reset all quark parameters so that it is as if they do not exist.
 *
 *   void set_quark_parms(int iquark, int im0, int smear, int isolv, double nu,
 *                        double xi, double cR, double cT)
 *     For the quark with index iquark, set its physical parameters. These
 *     parameters are:
 *
 *       im0  -  Index of its mass parameters
 *       smear - Whether to smear fields when computing the Dirac operator
 *       isolv - Index of the solver to use for this quark mass
 *       nu    - aniso param nu
 *       xi    - aniso param xi
 *       cR    - aniso param cR (for the clover term)
 *       cT    - aniso param cT (for the clover term)
 *
 *       The ani parameters are internally stored in an array:
 *       ani[4] = {nu, xi, cR, cT}
 *
 *   void read_quark_parms(int iquark)
 *     Read the secion "[Quark <iquark>]" from the input file open in file
 *     handle stdio and initialise.
 *
 *   quark_parms_t quark_parms(int iquark)
 *     Get the parameters for quark iquark.
 *
 *******************************************************************************/

#define HADSPEC_PARAMS_C
#define OPENQCD_INTERNAL

#ifdef __cplusplus
extern "C" {
#endif

#include "hadspec.h"

#include "openqcd/c_headers/flags.h"
#include "openqcd/c_headers/global.h"
#include "openqcd/c_headers/utils.h"

#ifdef __cplusplus
}
#endif

#include <mpi.h>

static int init = 0;
static quark_parms_t qp[MAX_QUARK_CONSTITUENTS + 1] = {
    {0, 0, 0, {0.0, 0.0, 0.0, 0.0}}};

/* Initialise the quark parameters to be empty */
static void init_qp(void)
{
  int i;

  for (i = 1; i <= MAX_QUARK_CONSTITUENTS; i++) {
    qp[i] = qp[0];
  }

  init = 1;
}

void reset_quark_parms(void)
{
  int i;

  if (init == 0) {
    init_qp();
    return;
  }

  for (i = 0; i < MAX_QUARK_CONSTITUENTS; i++) {
    qp[i] = qp[MAX_QUARK_CONSTITUENTS];
  }
}

void set_quark_parms(int iquark, int im0, int smear, int isolv, double nu,
                     double xi, double cR, double cT)
{
  int ie, iprms[3];
  double dprms[4];

  if (init == 0) {
    init_qp();
  }

  if (NPROC > 1) {
    iprms[0] = im0;
    iprms[1] = smear;
    iprms[2] = isolv;

    dprms[0] = nu;
    dprms[1] = xi;
    dprms[2] = cR;
    dprms[3] = cT;

    MPI_Bcast(iprms, 3, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(dprms, 4, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    ie = 0;

    ie |= (iprms[0] != im0);
    ie |= (iprms[1] != smear);
    ie |= (iprms[2] != isolv);

    ie |= (dprms[0] != nu);
    ie |= (dprms[1] != xi);
    ie |= (dprms[2] != cR);
    ie |= (dprms[3] != cT);

    error(ie != 0, 1, "set_quark_parms [hadspec_parms.c]",
          "Parameters not global");
  }

  ie = 0;
  ie |= ((iquark < 0) || (iquark >= MAX_QUARK_CONSTITUENTS));

  error_root(ie != 0, 1, "set_quark_parms [hadspec_parms.c]",
             "Invalid source parameters");

  qp[iquark].im0 = im0;
  qp[iquark].smear = smear;
  qp[iquark].isolv = isolv;

  qp[iquark].ani[0] = nu;
  qp[iquark].ani[1] = xi;
  qp[iquark].ani[2] = cR;
  qp[iquark].ani[3] = cT;
}

void read_quark_parms(int iquark)
{
  int im0, smear, isolv, my_rank;
  double nu, xi, cR, cT;
  char line[NAME_SIZE];
  ani_params_t ani;

  ani = ani_parms();

  error(!ani_params_initialised(), 1, "read_quark_parms [hadspec_parms.c]",
        "Anisotropic parameters not set");

  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

  if (my_rank == 0) {
    sprintf(line, "Quark %d", iquark);
    find_section(line);

    read_line("im0", "%d", &im0);
    read_optional_line("ani", "%lf %lf %lf %lf", &nu, ani.nu, &xi, ani.xi, &cR,
                       ani.cR, &cT, ani.cT);
    read_optional_line("smear", "%d", &smear, 0);
    read_optional_line("isolv", "%d", &isolv, 0);
  }

  if (NPROC > 1) {
    MPI_Bcast(&im0, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&smear, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&isolv, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&nu, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&xi, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&cR, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&cT, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  }

  set_quark_parms(iquark, im0, smear, isolv, nu, xi, cR, cT);
}

quark_parms_t quark_parms(int iquark)
{
  if (init == 0) {
    init_qp();
  }

  if ((iquark >= 0) && (iquark < MAX_QUARK_CONSTITUENTS)) {
    return qp[iquark];
  } else {
    error_loc(1, 1, "quark_parms [hadspec_parms.c]",
              "Quark index is out of range");
    return qp[MAX_QUARK_CONSTITUENTS];
  }
}
