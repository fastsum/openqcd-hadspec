/*******************************************************************************
 *
 * File hadspec_combinations.c
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * The externally accessible functions are
 *   void init_hadspec_combinations(int *quark_list, char *quark_names, int num)
 *     Initialise the list of all possible hadronic and mesonic combinations
 *     possible to construct with the quarks as specifiec by the quark list.
 *     Their names (1 character names) are stored in quark names. For example if
 *     one has 2 flavours one can create two different 1 flavour baryons, and
 *     two possible 2-flavour baryons (1-1-2 and 2-2-1). All of these
 *     combinations are computed once and stored for later reference. The method
 *     handles degenerate quark masses so that no baryon is ever computed
 *     multiple times.
 *
 *   int *baryon_1flavour_list(int *num)
 *     Return a list of indices of 1 flavour baryons.
 *
 *   char *baryon_1flavour_names(int *num)
 *     Return a list of names for 1 flavour baryons.
 *
 *   int **baryon_2flavour_list(int *num)
 *     Return a list of indices for 2 flavour baryon combinations, array is of
 *     dimension [num_combinations][2].
 *
 *   char **baryon_2flavour_names(int *num)
 *     Return a list of names for 2 flavour baryon combinations, array is of
 *     dimension [num_combinations][2].
 *
 *   int **meson_list(int *num)
 *     Return a list of indices for (2 flavour) meson combinations. Array if of
 *     dimension [num_combinations][2].
 *
 *   char **meson_names(int *num)
 *     Return a list of names meson combinations.
 *
 *******************************************************************************/

#define HADSPEC_COMBINATIONS_C
#define OPENQCD_INTERNAL

#ifdef __cplusplus
extern "C" {
#endif

#include "hadspec.h"

#include "openqcd/c_headers/utils.h"

#ifdef __cplusplus
}
#endif

#include <stdio.h>
#include <stdlib.h>

static int init = 0;
static int num_baryon_1fl, num_baryon_2fl, num_meson;
static int *baryon_1fl_l, **baryon_2fl_l, **meson_l;
static char *baryon_1fl_n, **baryon_2fl_n, **meson_n;

void init_hadspec_combinations(int *quark_list, char *quark_names, int num)
{
  int i, j, idx;
  int *unique_quarks, num_unique = 0;
  int **degen_quarks, num_degen = 0;

  unique_quarks = (int *)malloc(num * sizeof(*unique_quarks));
  degen_quarks = (int **)malloc((num - 1) * sizeof(*degen_quarks));

  error((degen_quarks == NULL) || (unique_quarks == NULL), 1,
        "init_hadspec_combinations [hadspec_combinations.c]",
        "Unable to initialise combination array");

  degen_quarks[0] = (int *)malloc(2 * (num - 1) * sizeof(**degen_quarks));

  error(degen_quarks[0] == NULL, 1,
        "init_hadspec_combinations [hadspec_combinations.c]",
        "Unable to initialise combination array");

  for (i = 1; i < (num - 1); ++i) {
    degen_quarks[i] = degen_quarks[i - 1] + 2;
  }

  /* Make a list of unique and degenerate quark masses */
  for (i = 0; i < num; ++i) {
    for (j = 0; j < num_unique; ++j) {
      if (quark_list[i] == quark_list[unique_quarks[j]]) {
        break;
      }
    }

    if (j == num_unique) {
      unique_quarks[num_unique] = i;
      num_unique += 1;
    } else {
      degen_quarks[num_degen][0] = unique_quarks[j];
      degen_quarks[num_degen][1] = i;
      num_degen += 1;
    }
  }

  /* Allocate index arrays for the mesons and baryons */
  num_baryon_1fl = num_unique;
  num_baryon_2fl = num_unique * (num_unique - 1) + num_degen;
  num_meson = num_unique * (num_unique + 1) / 2;

  baryon_1fl_l = (int *)malloc(num_baryon_1fl * sizeof(*baryon_1fl_l));
  baryon_1fl_n = (char *)malloc(num_baryon_1fl * sizeof(*baryon_1fl_n));

  baryon_2fl_l = (int **)malloc(num_baryon_2fl * sizeof(*baryon_2fl_l));
  baryon_2fl_n = (char **)malloc(num_baryon_2fl * sizeof(*baryon_2fl_n));

  error((baryon_1fl_l == NULL) || (baryon_1fl_n == NULL) ||
            (baryon_2fl_l == NULL) || (baryon_2fl_n == NULL),
        1, "init_hadspec_combinations [hadspec_combinations.c]",
        "Unable to initialise combination array");

  baryon_2fl_l[0] = (int *)malloc(2 * num_baryon_2fl * sizeof(**baryon_2fl_l));
  baryon_2fl_n[0] = (char *)malloc(2 * num_baryon_2fl * sizeof(**baryon_2fl_n));

  error((baryon_2fl_l[0] == NULL) || (baryon_2fl_n[0] == NULL), 1,
        "init_hadspec_combinations [hadspec_combinations.c]",
        "Unable to initialise combination array");

  for (i = 1; i < num_baryon_2fl; ++i) {
    baryon_2fl_l[i] = baryon_2fl_l[i - 1] + 2;
    baryon_2fl_n[i] = baryon_2fl_n[i - 1] + 2;
  }

  meson_l = (int **)malloc(num_meson * sizeof(*meson_l));
  meson_n = (char **)malloc(num_meson * sizeof(*meson_n));

  error((meson_l == NULL) || (meson_n == NULL), 1,
        "init_hadspec_combinations [hadspec_combinations.c]",
        "Unable to initialise combination array");

  meson_l[0] = (int *)malloc(2 * num_meson * sizeof(**meson_l));
  meson_n[0] = (char *)malloc(2 * num_meson * sizeof(**meson_n));

  error((meson_l[0] == NULL) || (meson_n[0] == NULL), 1,
        "init_hadspec_combinations [hadspec_combinations.c]",
        "Unable to initialise combination array");

  for (i = 1; i < num_meson; ++i) {
    meson_l[i] = meson_l[i - 1] + 2;
    meson_n[i] = meson_n[i - 1] + 2;
  }

  /* Fill 1 flavour baryon list */
  for (i = 0; i < num_unique; ++i) {
    baryon_1fl_l[i] = quark_list[unique_quarks[i]];
    baryon_1fl_n[i] = quark_names[unique_quarks[i]];
  }

  /* Fill meson list */
  for (i = 0, idx = 0; i < num_unique; ++i) {
    for (j = i; j < num_unique; ++j) {
      meson_l[idx][0] = quark_list[unique_quarks[i]];
      meson_l[idx][1] = quark_list[unique_quarks[j]];

      meson_n[idx][0] = quark_names[unique_quarks[i]];
      meson_n[idx][1] = quark_names[unique_quarks[j]];

      ++idx;
    }
  }

  /* Fill 2 flavour baryon list */
  for (i = 0, idx = 0; i < num_unique; ++i) {
    for (j = 0; j < num_unique; ++j) {
      if (i != j) {
        baryon_2fl_l[idx][0] = quark_list[unique_quarks[i]];
        baryon_2fl_l[idx][1] = quark_list[unique_quarks[j]];

        baryon_2fl_n[idx][0] = quark_names[unique_quarks[i]];
        baryon_2fl_n[idx][1] = quark_names[unique_quarks[j]];

        ++idx;
      }
    }
  }

  for (i = 0; i < num_degen; ++i) {
    baryon_2fl_l[idx + i][0] = quark_list[degen_quarks[i][0]];
    baryon_2fl_l[idx + i][1] = quark_list[degen_quarks[i][1]];

    baryon_2fl_n[idx + i][0] = quark_names[degen_quarks[i][0]];
    baryon_2fl_n[idx + i][1] = quark_names[degen_quarks[i][1]];
  }

  init = 1;

  free(unique_quarks);
  free(degen_quarks[0]);
  free(degen_quarks);
}

int *baryon_1flavour_list(int *num)
{
  error(init == 0, 1, "baryon_1flavour_list [hadspec_combinations.c]",
        "Hadron combinations not initialised");

  *num = num_baryon_1fl;
  return baryon_1fl_l;
}

char *baryon_1flavour_names(int *num)
{
  error(init == 0, 1, "baryon_1flavour_names [hadspec_combinations.c]",
        "Hadron combinations not initialised");

  *num = num_baryon_1fl;
  return baryon_1fl_n;
}

int **baryon_2flavour_list(int *num)
{
  error(init == 0, 1, "baryon_2flavour_list [hadspec_combinations.c]",
        "Hadron combinations not initialised");

  *num = num_baryon_2fl;
  return baryon_2fl_l;
}

char **baryon_2flavour_names(int *num)
{
  error(init == 0, 1, "baryon_2flavour_names [hadspec_combinations.c]",
        "Hadron combinations not initialised");

  *num = num_baryon_2fl;
  return baryon_2fl_n;
}

int **meson_list(int *num)
{
  error(init == 0, 1, "meson_list [hadspec_combinations.c]",
        "Hadron combinations not initialised");

  *num = num_meson;
  return meson_l;
}

char **meson_names(int *num)
{
  error(init == 0, 1, "meson_names [hadspec_combinations.c]",
        "Hadron combinations not initialised");

  *num = num_meson;
  return meson_n;
}
