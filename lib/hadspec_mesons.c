/*******************************************************************************
 *
 * File hadspec_mesons.c
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * The externally accessible functions are
 *
 *   size_t meson_size(void)
 *     Returns the size of the buffer necessary to store the result for
 *     computing the mesons.
 *
 *   void meson(int iquark1, int iquark2, complex_dble *out)
 *     Compute the 2 flavour meson state using the quarks specificed by
 *     iquark1 and iquark2, the result is stored in out which is expected to
 *     be of size meson_size.
 *
 * Notes:
 *
 *   The functions are more or less copy-pasted from the one-all contribution
 *   calculation in the mu-squared code. Ideally I would collect these methods
 *   in e.g. openqcd-propagator (although they do not really belong there), just
 *   to make them have a common base. Might be worthwhile in the future to take
 *   the functionality from openqcd-propagator and put it into openqcd-utilities
 *   or something and have propagator also be a non-library code.
 *
 *   Anyway, if any of these are optimised, the other should be as well.
 *
 *******************************************************************************/

#define HADSPEC_MESONS_C
#define OPENQCD_INTERNAL

#ifdef __cplusplus
extern "C" {
#endif

#include "gamma_prod.h"
#include "hadspec.h"

#include "openqcd/c_headers/global.h"
#include "openqcd/c_headers/linalg.h"
#include "openqcd/c_headers/sflds.h"
#include "openqcd/c_headers/utils.h"

#ifdef __cplusplus
}
#endif

#include <mpi.h>

static MPI_Comm *spatial_mpi_communicator = NULL;

typedef union
{
  spinor_dble s;
  complex_dble c[12];
} prop_t;

/* Make MPI communication groups so that we can do sums over space but not time
 * accross processes */
static void allocate_spatial_MPI_group(int my_rank)
{
  spatial_mpi_communicator =
      (MPI_Comm *)malloc(sizeof(*spatial_mpi_communicator));
  MPI_Comm_split(MPI_COMM_WORLD, cpr[0], my_rank, spatial_mpi_communicator);
}

/* Swap x and y and do the complex conjugate */
static void swap_conjugate(complex_dble *x, complex_dble *y)
{
  complex_dble tmp = *y;

  (*y).re = (*x).re;
  (*y).im = -(*x).im;

  (*x).re = tmp.re;
  (*x).im = -tmp.im;
}

/* Replace s by s^{dagger} for a 12x12 spin-colour matrix */
static void prop_dagger(prop_t *s)
{
  int sci, scj;

  for (sci = 0; sci < 12; ++sci) {
    for (scj = sci; scj < 12; ++scj) {
      swap_conjugate(&(s[sci].c[scj]), &(s[scj].c[sci]));
    }
  }
}

size_t meson_size(void)
{
  return sizeof(complex_dble) * 16 * L0;
}

void meson(int iquark1, int iquark2, complex_dble *out)
{
  int my_rank;
  int ix, it, ig, sci, scj;
  spinor_dble **prop1, **prop2;
  complex_dble *sum_buffer;
  prop_t bufferA[12] ALIGNED32;
  prop_t bufferB[12] ALIGNED32;

  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

  if (spatial_mpi_communicator == NULL) {
    allocate_spatial_MPI_group(my_rank);
  }

  prop1 = quark_propagators(iquark1);
  prop2 = quark_propagators(iquark2);

  sum_buffer = (complex_dble *)amalloc(16 * L0 * sizeof(*sum_buffer), ALIGN);

  for (it = 0; it < 16 * L0; ++it) {
    sum_buffer[it].re = 0.0;
    sum_buffer[it].im = 0.0;
  }

  /* Loop over the lattice volume */
  for (ix = 0; ix < VOLUME; ++ix) {

    it = ix / (L1 * L2 * L3);

    /* Loop over the 16 gamma matrices */
    for (ig = 0; ig < 16; ++ig) {

      /* Get buffers and right multiply A by g5G^dagger */
      for (sci = 0; sci < 12; ++sci) {
        bufferA[sci].s = prop1[sci][ipt[ix]];
        g5_gamma_dag_prod(&(bufferA[sci].s), ig);

        bufferB[sci].s = prop2[sci][ipt[ix]];
      }

      /* Conjugate transpose the B matrix */
      prop_dagger(bufferB);

      /* Left multiply B with G*g5 */
      for (sci = 0; sci < 12; ++sci) {
        gamma_g5_prod(&(bufferB[sci].s), ig);
      }

      /* Compute the spin/colour-trace */
      for (sci = 0; sci < 12; ++sci) {
        for (scj = 0; scj < 12; ++scj) {
          sum_buffer[16 * it + ig].re +=
              bufferA[sci].c[scj].re * bufferB[scj].c[sci].re -
              bufferA[sci].c[scj].im * bufferB[scj].c[sci].im;
          sum_buffer[16 * it + ig].im +=
              bufferA[sci].c[scj].im * bufferB[scj].c[sci].re +
              bufferA[sci].c[scj].re * bufferB[scj].c[sci].im;
        }
      }
    }
  }

  /* Sum over the spatial subprocesses */
  MPI_Allreduce(sum_buffer, out, 2 * 16 * L0, MPI_DOUBLE, MPI_SUM,
                *spatial_mpi_communicator);

  afree(sum_buffer);
}
