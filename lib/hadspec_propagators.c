/*******************************************************************************
 *
 * File hadspec_propagators.c
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * The externally accessible functions are
 *
 *   void init_propagators(int *quark_ids, int num)
 *     Given an array of quark indices, allocate memory for every non-degenerate
 *     quark flavour. Also create a mapping from quark indices to propagator
 *     indices so that one can refer to the right propagator even with indices
 *     that refer to degenerate quarks.
 *
 *   spinor_dble **quark_propagators(int iquark)
 *     Return the propagator relating to quark with index iquark.
 *
 *   timing_info_t compute_quark_propagator(int isrc, int iquark, int *status)
 *     For the quark at index iquark and the source with index isrc, compute the
 *     propagator and place the result in the appropriate propagator storage.
 *     Status of the inversion stored in status and the program returns timing
 *     informations on the inversions.
 *
 *******************************************************************************/

#define HADSPEC_PROPAGATORS_C
#define OPENQCD_INTERNAL

#ifdef __cplusplus
extern "C" {
#endif

#include "hadspec.h"

#include "openqcd/c_headers/linalg.h"
#include "openqcd/c_headers/sflds.h"
#include "openqcd/c_headers/stout_smearing.h"
#include "openqcd/c_headers/utils.h"

#include "openqcd-propagator/c_headers/propagator.h"
#include "openqcd-propagator/c_headers/smearing.h"
#include "openqcd-propagator/c_headers/sources.h"

#ifdef __cplusplus
}
#endif

static spinor_dble **pbuffer = NULL;
static int init = 0;
static int prop_map[MAX_QUARK_CONSTITUENTS + 1] = {-1};

/* Allocate memory for {num} number of propagator fields. Every propagator field
 * gets a single fermionic buffer */
static void allocate_quark_propagator_buffer(int num)
{
  int i;

  error(iup[0][0] == 0, 1,
        "allocate_quark_propagator_buffer [hadspec_buffer.c]",
        "Geometry arrays are not set");

  pbuffer = (spinor_dble **)malloc(num * 12 * sizeof(*pbuffer));

  error(pbuffer == NULL, 1,
        "allocate_quark_propagator_buffer [hadspec_buffer.c]",
        "Unable to allocate buffer field pointer array");

  pbuffer[0] = (spinor_dble *)amalloc(
      num * (12 * VOLUME + BNDRY / 2) * sizeof(**pbuffer), ALIGN);

  error(pbuffer[0] == NULL, 1, "allocate_propagator_field [propagator_field.c]",
        "Unable to allocate memory space for the propagator fields");

  for (i = 1; i < (num * 12); i++) {
    pbuffer[i] = pbuffer[i - 1] + VOLUME;

    if (num % 12 == 0) {
      pbuffer[i] += BNDRY / 2;
    }
  }
}

void init_propagators(int *quark_ids, int num)
{
  int i, num_unique = 0;

  error(init != 0, 1, "init_propagator_map [hadspec_propagators.c]",
        "Propagator map already initialised");

  for (i = 1; i < (MAX_QUARK_CONSTITUENTS + 1); ++i) {
    prop_map[i] = prop_map[0];
  }

  for (i = 0; i < num; ++i) {
    if (prop_map[quark_ids[i]] != -1) {
      continue;
    }

    prop_map[quark_ids[i]] = num_unique;
    num_unique += 1;
  }

  allocate_quark_propagator_buffer(num_unique);

  init = 1;
}

spinor_dble **quark_propagators(int iquark)
{
  error(init == 0, 1, "quark_propagators [hadspec_propagators.c]",
        "Propagator memory not initialised");

  error((iquark < 0) || (iquark >= MAX_QUARK_CONSTITUENTS) ||
            prop_map[iquark] == -1,
        1, "quark_propagators [hadspec_propagators.c]", "Invalid quark id");

  return pbuffer + 12 * prop_map[iquark];
}

timing_info_t compute_quark_propagator(int isrc, int iquark, int *status)
{
  int l, sci, stat[3];
  double start_time;
  source_parms_t sp;
  quark_parms_t qp;
  spinor_dble **prop, **wsd, *eta;
  timing_info_t tinfo = {0.0, 0.0, 0};

  sp = source_parms(isrc);
  qp = quark_parms(iquark);
  prop = quark_propagators(iquark);

  reset_ani_parms(0, qp.ani[0], qp.ani[1], qp.ani[2], qp.ani[3], 1.0, 1.0, 1.0,
                  1.0);
  set_sw_parms(sea_quark_mass(qp.im0));

  wsd = reserve_wsd(1);
  eta = wsd[0];

  for (sci = 0; sci < 12; ++sci) {
    start_time = start_timer();

    /* Generate the source */
    generate_source(isrc, sci, eta);

    if (qp.smear) {
      smear_fields();
    }

    solve_dirac(eta, prop[sci], qp.isolv, stat);

    for (l = 0; l < 2; l++) {
      status[l] += stat[l];
    }

    /* Sink smearing */
    if (sp.type == GAUSSIAN_SMEARED_SOURCE) {
      gaussian_smearing(prop[sci], sp.num_application, sp.smear_parameter);
    }

    if (qp.smear) {
      unsmear_fields();
    }

    stop_timer(&tinfo, start_time);

    for (l = 0; l < 2; l++) {
      status[l] += stat[l];
    }

    status[2] += (stat[2] != 0);
  }

  for (l = 0; l < 2; l++) {
    status[l] = (status[l] + (12 / 2)) / 12;
  }

  release_wsd();
  return tinfo;
}
