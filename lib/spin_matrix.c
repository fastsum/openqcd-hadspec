/*******************************************************************************
 *
 * File spin_matrix.c
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Functions that act on 4x4 spin matrices.
 *
 * The externally accessible functions are
 *   spin_matrix transpose_spin_matrix(spin_matrix const *sm)
 *     Return the transpose of spin matrix sm
 *
 *   complex_dble trace_spin_matrix(spin_matrix const *sm)
 *     Return the trace of spin matrix sm
 *
 *   spin_matrix mulc_spin_matrix(complex_dble c, spin_matrix const *sm)
 *     Return the c * sm for a constant c and spin matrix sm
 *
 *   spin_matrix mul_spin_spin_matrix(spin_matrix const *smA,
 *                                    spin_matrix const *smB)
 *     Return the matrix multiplication of two spin matrices
 *
 *   spin_matrix mul_spin_spin_matrixransposed_matrix(spin_matrix const *smA,
 *                                                    spin_matrix const *smB)
 *     Return the matrix multiplication of smA * (smB)^T, the second matrix
 *     transposed
 *
 *   spin_matrix mul_charge_gamma(spin_matrix const *sm, int g)
 *     Return the matrix multiplication of C gamma(g) * sm, where C is the
 *     charge conjugation operator and gamma(g) is the g'th gamma matrix
 *
 *   complex_dble mul_parity_project_trace(spin_matrix const *sm, int p)
 *     Return tr (sm * P^+) where P^+ is the positive parity projection operator
 *
 *   complex_dble (*mul_spin_parity_project_trace[4][4])(spin_matrix const *sm)
 *     Return tr (sm * P_{mu,nu} P^+) where P_{mu,nu} is a spin state projection
 *     operator and P^+ is the positive parity projection operator

 *   prop_matrix fetch_prop_matrix(spinor_dble **prop, int ipt)
 *     Given a propagator field, fetch a single propagator matrix (12x12
 *     spin-colour matrix) at space-time position ipt
 *
 *   spin_matrix fetch_spin_matrix(prop_matrix *prop, int ia, int ib)
 *     For a propagator matrix, fetch a spin matrix given the colour indices ia
 *     and ib
 *
 *******************************************************************************/

#define SPIN_MATRIX_C
#define OPENQCD_INTERNAL

#ifdef __cplusplus
extern "C" {
#endif

#include "spin_matrix.h"

#ifdef __cplusplus
}
#endif

#include <string.h>

/* Multiply a row of a 4x4 spin matrix to the right by C g0 */
static void mul_charge_gamma_row_g0(complex_dble const *in, complex_dble *out)
{
  out[0].re = -in[3].im;
  out[0].im = in[3].re;
  out[1].re = in[2].im;
  out[1].im = -in[2].re;
  out[2].re = in[1].im;
  out[2].im = -in[1].re;
  out[3].re = -in[0].im;
  out[3].im = in[0].re;
}

/* Multiply a row of a 4x4 spin matrix to the right by C g1 */
static void mul_charge_gamma_row_g1(complex_dble const *in, complex_dble *out)
{
  out[0].re = -in[2].re;
  out[0].im = -in[2].im;
  out[1] = in[3];
  out[2].re = -in[0].re;
  out[2].im = -in[0].im;
  out[3] = in[1];
}

/* Multiply a row of a 4x4 spin matrix to the right by C g2 */
static void mul_charge_gamma_row_g2(complex_dble const *in, complex_dble *out)
{
  out[0].re = in[2].im;
  out[0].im = -in[2].re;
  out[1].re = in[3].im;
  out[1].im = -in[3].re;
  out[2].re = in[0].im;
  out[2].im = -in[0].re;
  out[3].re = in[1].im;
  out[3].im = -in[1].re;
}

/* Multiply a row of a 4x4 spin matrix to the right by C g3 */
static void mul_charge_gamma_row_g3(complex_dble const *in, complex_dble *out)
{
  out[0] = in[3];
  out[1] = in[2];
  out[2] = in[1];
  out[3] = in[0];
}

/* Multiply a row of a 4x4 spin matrix to the right by C g5 */
static void mul_charge_gamma_row_g5(complex_dble const *in, complex_dble *out)
{
  out[0].re = -in[1].im;
  out[0].im = in[1].re;
  out[1].re = in[0].im;
  out[1].im = -in[0].re;
  out[2].re = -in[3].im;
  out[2].im = in[3].re;
  out[3].re = in[2].im;
  out[3].im = -in[2].re;
}

static void (*mul_charge_gamma_row[5])(complex_dble const *in,
                                       complex_dble *out) = {
    mul_charge_gamma_row_g0, mul_charge_gamma_row_g1, mul_charge_gamma_row_g2,
    mul_charge_gamma_row_g3, mul_charge_gamma_row_g5};

/* Compute tr(sm P_00 P_+) for spin and parity projection operator */
static complex_dble mul_spin_00_parity_project_trace(spin_matrix const *sm)
{
  complex_dble result;

  result.re = (*sm).s[0][0].re / 3 - (*sm).s[0][2].re / 3 +
              (*sm).s[1][1].re / 3 - (*sm).s[1][3].re / 3 -
              (*sm).s[2][0].re / 3 + (*sm).s[2][2].re / 3 -
              (*sm).s[3][1].re / 3 + (*sm).s[3][3].re / 3;

  result.im = (*sm).s[0][0].im / 3 - (*sm).s[0][2].im / 3 +
              (*sm).s[1][1].im / 3 - (*sm).s[1][3].im / 3 -
              (*sm).s[2][0].im / 3 + (*sm).s[2][2].im / 3 -
              (*sm).s[3][1].im / 3 + (*sm).s[3][3].im / 3;
  return result;
}

/* Compute tr(sm P_01 P_+) for spin and parity projection operator */
static complex_dble mul_spin_01_parity_project_trace(spin_matrix const *sm)
{
  complex_dble result;
  result.re =
      (*sm).s[0][0].re / 2 - (*sm).s[0][1].im / 6 - (*sm).s[0][2].re / 2 -
      (*sm).s[0][3].im / 6 - (*sm).s[1][0].im / 6 + (*sm).s[1][1].re / 2 -
      (*sm).s[1][2].im / 6 - (*sm).s[1][3].re / 2 - (*sm).s[2][0].re / 2 +
      (*sm).s[2][1].im / 6 + (*sm).s[2][2].re / 2 + (*sm).s[2][3].im / 6 +
      (*sm).s[3][0].im / 6 - (*sm).s[3][1].re / 2 + (*sm).s[3][2].im / 6 +
      (*sm).s[3][3].re / 2;
  result.im =
      (*sm).s[0][0].im / 2 + (*sm).s[0][1].re / 6 - (*sm).s[0][2].im / 2 +
      (*sm).s[0][3].re / 6 + (*sm).s[1][0].re / 6 + (*sm).s[1][1].im / 2 +
      (*sm).s[1][2].re / 6 - (*sm).s[1][3].im / 2 - (*sm).s[2][0].im / 2 -
      (*sm).s[2][1].re / 6 + (*sm).s[2][2].im / 2 - (*sm).s[2][3].re / 6 -
      (*sm).s[3][0].re / 6 - (*sm).s[3][1].im / 2 - (*sm).s[3][2].re / 6 +
      (*sm).s[3][3].im / 2;
  return result;
}

/* Compute tr(sm P_02 P_+) for spin and parity projection operator */
static complex_dble mul_spin_02_parity_project_trace(spin_matrix const *sm)
{
  complex_dble result;
  result.re =
      (*sm).s[0][0].re / 2 - (*sm).s[0][1].re / 6 - (*sm).s[0][2].re / 2 -
      (*sm).s[0][3].re / 6 + (*sm).s[1][0].re / 6 + (*sm).s[1][1].re / 2 +
      (*sm).s[1][2].re / 6 - (*sm).s[1][3].re / 2 - (*sm).s[2][0].re / 2 +
      (*sm).s[2][1].re / 6 + (*sm).s[2][2].re / 2 + (*sm).s[2][3].re / 6 -
      (*sm).s[3][0].re / 6 - (*sm).s[3][1].re / 2 - (*sm).s[3][2].re / 6 +
      (*sm).s[3][3].re / 2;
  result.im =
      (*sm).s[0][0].im / 2 - (*sm).s[0][1].im / 6 - (*sm).s[0][2].im / 2 -
      (*sm).s[0][3].im / 6 + (*sm).s[1][0].im / 6 + (*sm).s[1][1].im / 2 +
      (*sm).s[1][2].im / 6 - (*sm).s[1][3].im / 2 - (*sm).s[2][0].im / 2 +
      (*sm).s[2][1].im / 6 + (*sm).s[2][2].im / 2 + (*sm).s[2][3].im / 6 -
      (*sm).s[3][0].im / 6 - (*sm).s[3][1].im / 2 - (*sm).s[3][2].im / 6 +
      (*sm).s[3][3].im / 2;
  return result;
}

/* Compute tr(sm P_03 P_+) for spin and parity projection operator */
static complex_dble mul_spin_03_parity_project_trace(spin_matrix const *sm)
{
  complex_dble result;
  result.re =
      -((*sm).s[0][0].im / 6) + (*sm).s[0][0].re / 2 - (*sm).s[0][2].im / 6 -
      (*sm).s[0][2].re / 2 + (*sm).s[1][1].im / 6 + (*sm).s[1][1].re / 2 +
      (*sm).s[1][3].im / 6 - (*sm).s[1][3].re / 2 + (*sm).s[2][0].im / 6 -
      (*sm).s[2][0].re / 2 + (*sm).s[2][2].im / 6 + (*sm).s[2][2].re / 2 -
      (*sm).s[3][1].im / 6 - (*sm).s[3][1].re / 2 - (*sm).s[3][3].im / 6 +
      (*sm).s[3][3].re / 2;
  result.im =
      (*sm).s[0][0].im / 2 + (*sm).s[0][0].re / 6 - (*sm).s[0][2].im / 2 +
      (*sm).s[0][2].re / 6 + (*sm).s[1][1].im / 2 - (*sm).s[1][1].re / 6 -
      (*sm).s[1][3].im / 2 - (*sm).s[1][3].re / 6 - (*sm).s[2][0].im / 2 -
      (*sm).s[2][0].re / 6 + (*sm).s[2][2].im / 2 - (*sm).s[2][2].re / 6 -
      (*sm).s[3][1].im / 2 + (*sm).s[3][1].re / 6 + (*sm).s[3][3].im / 2 +
      (*sm).s[3][3].re / 6;
  return result;
}

/* Compute tr(sm P_10 P_+) for spin and parity projection operator */
static complex_dble mul_spin_10_parity_project_trace(spin_matrix const *sm)
{
  complex_dble result;
  result.re =
      (*sm).s[0][0].re / 2 + (*sm).s[0][1].im / 6 - (*sm).s[0][2].re / 2 +
      (*sm).s[0][3].im / 6 + (*sm).s[1][0].im / 6 + (*sm).s[1][1].re / 2 +
      (*sm).s[1][2].im / 6 - (*sm).s[1][3].re / 2 - (*sm).s[2][0].re / 2 -
      (*sm).s[2][1].im / 6 + (*sm).s[2][2].re / 2 - (*sm).s[2][3].im / 6 -
      (*sm).s[3][0].im / 6 - (*sm).s[3][1].re / 2 - (*sm).s[3][2].im / 6 +
      (*sm).s[3][3].re / 2;
  result.im =
      (*sm).s[0][0].im / 2 - (*sm).s[0][1].re / 6 - (*sm).s[0][2].im / 2 -
      (*sm).s[0][3].re / 6 - (*sm).s[1][0].re / 6 + (*sm).s[1][1].im / 2 -
      (*sm).s[1][2].re / 6 - (*sm).s[1][3].im / 2 - (*sm).s[2][0].im / 2 +
      (*sm).s[2][1].re / 6 + (*sm).s[2][2].im / 2 + (*sm).s[2][3].re / 6 +
      (*sm).s[3][0].re / 6 - (*sm).s[3][1].im / 2 + (*sm).s[3][2].re / 6 +
      (*sm).s[3][3].im / 2;
  return result;
}

/* Compute tr(sm P_11 P_+) for spin and parity projection operator */
static complex_dble mul_spin_11_parity_project_trace(spin_matrix const *sm)
{
  complex_dble result;
  result.re = (*sm).s[0][0].re / 3 - (*sm).s[0][2].re / 3 +
              (*sm).s[1][1].re / 3 - (*sm).s[1][3].re / 3 -
              (*sm).s[2][0].re / 3 + (*sm).s[2][2].re / 3 -
              (*sm).s[3][1].re / 3 + (*sm).s[3][3].re / 3;
  result.im = (*sm).s[0][0].im / 3 - (*sm).s[0][2].im / 3 +
              (*sm).s[1][1].im / 3 - (*sm).s[1][3].im / 3 -
              (*sm).s[2][0].im / 3 + (*sm).s[2][2].im / 3 -
              (*sm).s[3][1].im / 3 + (*sm).s[3][3].im / 3;
  return result;
}

/* Compute tr(sm P_12 P_+) for spin and parity projection operator */
static complex_dble mul_spin_12_parity_project_trace(spin_matrix const *sm)
{
  complex_dble result;
  result.re =
      (*sm).s[0][0].im / 6 + (*sm).s[0][0].re / 2 - (*sm).s[0][2].im / 6 -
      (*sm).s[0][2].re / 2 - (*sm).s[1][1].im / 6 + (*sm).s[1][1].re / 2 +
      (*sm).s[1][3].im / 6 - (*sm).s[1][3].re / 2 - (*sm).s[2][0].im / 6 -
      (*sm).s[2][0].re / 2 + (*sm).s[2][2].im / 6 + (*sm).s[2][2].re / 2 +
      (*sm).s[3][1].im / 6 - (*sm).s[3][1].re / 2 - (*sm).s[3][3].im / 6 +
      (*sm).s[3][3].re / 2;
  result.im =
      (*sm).s[0][0].im / 2 - (*sm).s[0][0].re / 6 - (*sm).s[0][2].im / 2 +
      (*sm).s[0][2].re / 6 + (*sm).s[1][1].im / 2 + (*sm).s[1][1].re / 6 -
      (*sm).s[1][3].im / 2 - (*sm).s[1][3].re / 6 - (*sm).s[2][0].im / 2 +
      (*sm).s[2][0].re / 6 + (*sm).s[2][2].im / 2 - (*sm).s[2][2].re / 6 -
      (*sm).s[3][1].im / 2 - (*sm).s[3][1].re / 6 + (*sm).s[3][3].im / 2 +
      (*sm).s[3][3].re / 6;
  return result;
}

/* Compute tr(sm P_13 P_+) for spin and parity projection operator */
static complex_dble mul_spin_13_parity_project_trace(spin_matrix const *sm)
{
  complex_dble result;
  result.re =
      (*sm).s[0][0].re / 2 - (*sm).s[0][1].re / 6 - (*sm).s[0][2].re / 2 +
      (*sm).s[0][3].re / 6 + (*sm).s[1][0].re / 6 + (*sm).s[1][1].re / 2 -
      (*sm).s[1][2].re / 6 - (*sm).s[1][3].re / 2 - (*sm).s[2][0].re / 2 +
      (*sm).s[2][1].re / 6 + (*sm).s[2][2].re / 2 - (*sm).s[2][3].re / 6 -
      (*sm).s[3][0].re / 6 - (*sm).s[3][1].re / 2 + (*sm).s[3][2].re / 6 +
      (*sm).s[3][3].re / 2;
  result.im =
      (*sm).s[0][0].im / 2 - (*sm).s[0][1].im / 6 - (*sm).s[0][2].im / 2 +
      (*sm).s[0][3].im / 6 + (*sm).s[1][0].im / 6 + (*sm).s[1][1].im / 2 -
      (*sm).s[1][2].im / 6 - (*sm).s[1][3].im / 2 - (*sm).s[2][0].im / 2 +
      (*sm).s[2][1].im / 6 + (*sm).s[2][2].im / 2 - (*sm).s[2][3].im / 6 -
      (*sm).s[3][0].im / 6 - (*sm).s[3][1].im / 2 + (*sm).s[3][2].im / 6 +
      (*sm).s[3][3].im / 2;
  return result;
}

/* Compute tr(sm P_20 P_+) for spin and parity projection operator */
static complex_dble mul_spin_20_parity_project_trace(spin_matrix const *sm)
{
  complex_dble result;
  result.re =
      (*sm).s[0][0].re / 2 + (*sm).s[0][1].re / 6 - (*sm).s[0][2].re / 2 +
      (*sm).s[0][3].re / 6 - (*sm).s[1][0].re / 6 + (*sm).s[1][1].re / 2 -
      (*sm).s[1][2].re / 6 - (*sm).s[1][3].re / 2 - (*sm).s[2][0].re / 2 -
      (*sm).s[2][1].re / 6 + (*sm).s[2][2].re / 2 - (*sm).s[2][3].re / 6 +
      (*sm).s[3][0].re / 6 - (*sm).s[3][1].re / 2 + (*sm).s[3][2].re / 6 +
      (*sm).s[3][3].re / 2;
  result.im =
      (*sm).s[0][0].im / 2 + (*sm).s[0][1].im / 6 - (*sm).s[0][2].im / 2 +
      (*sm).s[0][3].im / 6 - (*sm).s[1][0].im / 6 + (*sm).s[1][1].im / 2 -
      (*sm).s[1][2].im / 6 - (*sm).s[1][3].im / 2 - (*sm).s[2][0].im / 2 -
      (*sm).s[2][1].im / 6 + (*sm).s[2][2].im / 2 - (*sm).s[2][3].im / 6 +
      (*sm).s[3][0].im / 6 - (*sm).s[3][1].im / 2 + (*sm).s[3][2].im / 6 +
      (*sm).s[3][3].im / 2;
  return result;
}

/* Compute tr(sm P_21 P_+) for spin and parity projection operator */
static complex_dble mul_spin_21_parity_project_trace(spin_matrix const *sm)
{
  complex_dble result;
  result.re =
      -((*sm).s[0][0].im / 6) + (*sm).s[0][0].re / 2 + (*sm).s[0][2].im / 6 -
      (*sm).s[0][2].re / 2 + (*sm).s[1][1].im / 6 + (*sm).s[1][1].re / 2 -
      (*sm).s[1][3].im / 6 - (*sm).s[1][3].re / 2 + (*sm).s[2][0].im / 6 -
      (*sm).s[2][0].re / 2 - (*sm).s[2][2].im / 6 + (*sm).s[2][2].re / 2 -
      (*sm).s[3][1].im / 6 - (*sm).s[3][1].re / 2 + (*sm).s[3][3].im / 6 +
      (*sm).s[3][3].re / 2;
  result.im =
      (*sm).s[0][0].im / 2 + (*sm).s[0][0].re / 6 - (*sm).s[0][2].im / 2 -
      (*sm).s[0][2].re / 6 + (*sm).s[1][1].im / 2 - (*sm).s[1][1].re / 6 -
      (*sm).s[1][3].im / 2 + (*sm).s[1][3].re / 6 - (*sm).s[2][0].im / 2 -
      (*sm).s[2][0].re / 6 + (*sm).s[2][2].im / 2 + (*sm).s[2][2].re / 6 -
      (*sm).s[3][1].im / 2 + (*sm).s[3][1].re / 6 + (*sm).s[3][3].im / 2 -
      (*sm).s[3][3].re / 6;
  return result;
}

/* Compute tr(sm P_22 P_+) for spin and parity projection operator */
static complex_dble mul_spin_22_parity_project_trace(spin_matrix const *sm)
{
  complex_dble result;
  result.re = (*sm).s[0][0].re / 3 - (*sm).s[0][2].re / 3 +
              (*sm).s[1][1].re / 3 - (*sm).s[1][3].re / 3 -
              (*sm).s[2][0].re / 3 + (*sm).s[2][2].re / 3 -
              (*sm).s[3][1].re / 3 + (*sm).s[3][3].re / 3;
  result.im = (*sm).s[0][0].im / 3 - (*sm).s[0][2].im / 3 +
              (*sm).s[1][1].im / 3 - (*sm).s[1][3].im / 3 -
              (*sm).s[2][0].im / 3 + (*sm).s[2][2].im / 3 -
              (*sm).s[3][1].im / 3 + (*sm).s[3][3].im / 3;
  return result;
}

/* Compute tr(sm P_23 P_+) for spin and parity projection operator */
static complex_dble mul_spin_23_parity_project_trace(spin_matrix const *sm)
{
  complex_dble result;
  result.re =
      (*sm).s[0][0].re / 2 + (*sm).s[0][1].im / 6 - (*sm).s[0][2].re / 2 -
      (*sm).s[0][3].im / 6 + (*sm).s[1][0].im / 6 + (*sm).s[1][1].re / 2 -
      (*sm).s[1][2].im / 6 - (*sm).s[1][3].re / 2 - (*sm).s[2][0].re / 2 -
      (*sm).s[2][1].im / 6 + (*sm).s[2][2].re / 2 + (*sm).s[2][3].im / 6 -
      (*sm).s[3][0].im / 6 - (*sm).s[3][1].re / 2 + (*sm).s[3][2].im / 6 +
      (*sm).s[3][3].re / 2;
  result.im =
      (*sm).s[0][0].im / 2 - (*sm).s[0][1].re / 6 - (*sm).s[0][2].im / 2 +
      (*sm).s[0][3].re / 6 - (*sm).s[1][0].re / 6 + (*sm).s[1][1].im / 2 +
      (*sm).s[1][2].re / 6 - (*sm).s[1][3].im / 2 - (*sm).s[2][0].im / 2 +
      (*sm).s[2][1].re / 6 + (*sm).s[2][2].im / 2 - (*sm).s[2][3].re / 6 +
      (*sm).s[3][0].re / 6 - (*sm).s[3][1].im / 2 - (*sm).s[3][2].re / 6 +
      (*sm).s[3][3].im / 2;
  return result;
}

/* Compute tr(sm P_30 P_+) for spin and parity projection operator */
static complex_dble mul_spin_30_parity_project_trace(spin_matrix const *sm)
{
  complex_dble result;
  result.re =
      (*sm).s[0][0].im / 6 + (*sm).s[0][0].re / 2 + (*sm).s[0][2].im / 6 -
      (*sm).s[0][2].re / 2 - (*sm).s[1][1].im / 6 + (*sm).s[1][1].re / 2 -
      (*sm).s[1][3].im / 6 - (*sm).s[1][3].re / 2 - (*sm).s[2][0].im / 6 -
      (*sm).s[2][0].re / 2 - (*sm).s[2][2].im / 6 + (*sm).s[2][2].re / 2 +
      (*sm).s[3][1].im / 6 - (*sm).s[3][1].re / 2 + (*sm).s[3][3].im / 6 +
      (*sm).s[3][3].re / 2;
  result.im =
      (*sm).s[0][0].im / 2 - (*sm).s[0][0].re / 6 - (*sm).s[0][2].im / 2 -
      (*sm).s[0][2].re / 6 + (*sm).s[1][1].im / 2 + (*sm).s[1][1].re / 6 -
      (*sm).s[1][3].im / 2 + (*sm).s[1][3].re / 6 - (*sm).s[2][0].im / 2 +
      (*sm).s[2][0].re / 6 + (*sm).s[2][2].im / 2 + (*sm).s[2][2].re / 6 -
      (*sm).s[3][1].im / 2 - (*sm).s[3][1].re / 6 + (*sm).s[3][3].im / 2 -
      (*sm).s[3][3].re / 6;
  return result;
}

/* Compute tr(sm P_31 P_+) for spin and parity projection operator */
static complex_dble mul_spin_31_parity_project_trace(spin_matrix const *sm)
{
  complex_dble result;
  result.re =
      (*sm).s[0][0].re / 2 + (*sm).s[0][1].re / 6 - (*sm).s[0][2].re / 2 -
      (*sm).s[0][3].re / 6 - (*sm).s[1][0].re / 6 + (*sm).s[1][1].re / 2 +
      (*sm).s[1][2].re / 6 - (*sm).s[1][3].re / 2 - (*sm).s[2][0].re / 2 -
      (*sm).s[2][1].re / 6 + (*sm).s[2][2].re / 2 + (*sm).s[2][3].re / 6 +
      (*sm).s[3][0].re / 6 - (*sm).s[3][1].re / 2 - (*sm).s[3][2].re / 6 +
      (*sm).s[3][3].re / 2;
  result.im =
      (*sm).s[0][0].im / 2 + (*sm).s[0][1].im / 6 - (*sm).s[0][2].im / 2 -
      (*sm).s[0][3].im / 6 - (*sm).s[1][0].im / 6 + (*sm).s[1][1].im / 2 +
      (*sm).s[1][2].im / 6 - (*sm).s[1][3].im / 2 - (*sm).s[2][0].im / 2 -
      (*sm).s[2][1].im / 6 + (*sm).s[2][2].im / 2 + (*sm).s[2][3].im / 6 +
      (*sm).s[3][0].im / 6 - (*sm).s[3][1].im / 2 - (*sm).s[3][2].im / 6 +
      (*sm).s[3][3].im / 2;
  return result;
}

/* Compute tr(sm P_32 P_+) for spin and parity projection operator */
static complex_dble mul_spin_32_parity_project_trace(spin_matrix const *sm)
{
  complex_dble result;
  result.re =
      (*sm).s[0][0].re / 2 - (*sm).s[0][1].im / 6 - (*sm).s[0][2].re / 2 +
      (*sm).s[0][3].im / 6 - (*sm).s[1][0].im / 6 + (*sm).s[1][1].re / 2 +
      (*sm).s[1][2].im / 6 - (*sm).s[1][3].re / 2 - (*sm).s[2][0].re / 2 +
      (*sm).s[2][1].im / 6 + (*sm).s[2][2].re / 2 - (*sm).s[2][3].im / 6 +
      (*sm).s[3][0].im / 6 - (*sm).s[3][1].re / 2 - (*sm).s[3][2].im / 6 +
      (*sm).s[3][3].re / 2;
  result.im =
      (*sm).s[0][0].im / 2 + (*sm).s[0][1].re / 6 - (*sm).s[0][2].im / 2 -
      (*sm).s[0][3].re / 6 + (*sm).s[1][0].re / 6 + (*sm).s[1][1].im / 2 -
      (*sm).s[1][2].re / 6 - (*sm).s[1][3].im / 2 - (*sm).s[2][0].im / 2 -
      (*sm).s[2][1].re / 6 + (*sm).s[2][2].im / 2 + (*sm).s[2][3].re / 6 -
      (*sm).s[3][0].re / 6 - (*sm).s[3][1].im / 2 + (*sm).s[3][2].re / 6 +
      (*sm).s[3][3].im / 2;
  return result;
}

/* Compute tr(sm P_33 P_+) for spin and parity projection operator */
static complex_dble mul_spin_33_parity_project_trace(spin_matrix const *sm)
{
  complex_dble result;
  result.re = (*sm).s[0][0].re / 3 - (*sm).s[0][2].re / 3 +
              (*sm).s[1][1].re / 3 - (*sm).s[1][3].re / 3 -
              (*sm).s[2][0].re / 3 + (*sm).s[2][2].re / 3 -
              (*sm).s[3][1].re / 3 + (*sm).s[3][3].re / 3;
  result.im = (*sm).s[0][0].im / 3 - (*sm).s[0][2].im / 3 +
              (*sm).s[1][1].im / 3 - (*sm).s[1][3].im / 3 -
              (*sm).s[2][0].im / 3 + (*sm).s[2][2].im / 3 -
              (*sm).s[3][1].im / 3 + (*sm).s[3][3].im / 3;
  return result;
}

spin_matrix transpose_spin_matrix(spin_matrix const *sm)
{
  int i, j;
  spin_matrix result;

  for (i = 0; i < 4; ++i) {
    for (j = 0; j < 4; ++j) {
      result.s[i][j] = (*sm).s[j][i];
    }
  }

  return result;
}

complex_dble trace_spin_matrix(spin_matrix const *sm)
{
  int i;
  complex_dble result = {0.0, 0.0};

  for (i = 0; i < 4; ++i) {
    result.re += (*sm).s[i][i].re;
    result.im += (*sm).s[i][i].im;
  }

  return result;
}

spin_matrix mulc_spin_matrix(complex_dble c, spin_matrix const *sm)
{
  int i, j;
  spin_matrix result;

  for (i = 0; i < 4; ++i) {
    for (j = 0; j < 4; ++j) {
      result.s[i][j].re = c.re * (*sm).s[i][j].re - c.im * (*sm).s[i][j].im;
      result.s[i][j].im = c.re * (*sm).s[i][j].im + c.im * (*sm).s[i][j].re;
    }
  }

  return result;
}

/* Multiply two 4x4 spin matrices */
spin_matrix mul_spin_spin_matrix(spin_matrix const *smA, spin_matrix const *smB)
{
  int i, j, k;
  spin_matrix result;

  for (i = 0; i < 4; ++i) {
    for (j = 0; j < 4; ++j) {
      result.s[i][j].re = 0.0;
      result.s[i][j].im = 0.0;

      for (k = 0; k < 4; ++k) {
        result.s[i][j].re += (*smA).s[i][k].re * (*smB).s[k][j].re -
                             (*smA).s[i][k].im * (*smB).s[k][j].im;
        result.s[i][j].im += (*smA).s[i][k].re * (*smB).s[k][j].im +
                             (*smA).s[i][k].im * (*smB).s[k][j].re;
      }
    }
  }

  return result;
}

/* Multiply two 4x4 spin matrices where smB is transposed */
spin_matrix mul_spin_spin_matrixransposed_matrix(spin_matrix const *smA,
                                                 spin_matrix const *smB)
{
  int i, j, k;
  spin_matrix result;

  for (i = 0; i < 4; ++i) {
    for (j = 0; j < 4; ++j) {
      result.s[i][j].re = 0.0;
      result.s[i][j].im = 0.0;

      for (k = 0; k < 4; ++k) {
        result.s[i][j].re += (*smA).s[i][k].re * (*smB).s[j][k].re -
                             (*smA).s[i][k].im * (*smB).s[j][k].im;
        result.s[i][j].im += (*smA).s[i][k].re * (*smB).s[j][k].im +
                             (*smA).s[i][k].im * (*smB).s[j][k].re;
      }
    }
  }

  return result;
}

spin_matrix mul_charge_gamma(spin_matrix const *sm, int g)
{
  int row;
  spin_matrix result;

  for (row = 0; row < 4; ++row) {
    mul_charge_gamma_row[g]((*sm).s[row], result.s[row]);
  }

  return result;
}

complex_dble mul_parity_project_trace(spin_matrix const *sm, int p)
{
  complex_dble result;

  if (p == 0) {
    result.re = 0.5 * ((*sm).s[0][0].re - (*sm).s[0][2].re + (*sm).s[1][1].re -
                       (*sm).s[1][3].re - (*sm).s[2][0].re + (*sm).s[2][2].re -
                       (*sm).s[3][1].re + (*sm).s[3][3].re);

    result.im = 0.5 * ((*sm).s[0][0].im - (*sm).s[0][2].im + (*sm).s[1][1].im -
                       (*sm).s[1][3].im - (*sm).s[2][0].im + (*sm).s[2][2].im -
                       (*sm).s[3][1].im + (*sm).s[3][3].im);
  } else {
    result.re = 0.5 * ((*sm).s[0][0].re + (*sm).s[0][2].re + (*sm).s[1][1].re +
                       (*sm).s[1][3].re + (*sm).s[2][0].re + (*sm).s[2][2].re +
                       (*sm).s[3][1].re + (*sm).s[3][3].re);

    result.im = 0.5 * ((*sm).s[0][0].im + (*sm).s[0][2].im + (*sm).s[1][1].im +
                       (*sm).s[1][3].im + (*sm).s[2][0].im + (*sm).s[2][2].im +
                       (*sm).s[3][1].im + (*sm).s[3][3].im);
  }

  return result;
}

complex_dble (*mul_spin_parity_project_trace[4][4])(spin_matrix const *sm) = {
    {mul_spin_00_parity_project_trace, mul_spin_01_parity_project_trace,
     mul_spin_02_parity_project_trace, mul_spin_03_parity_project_trace},
    {mul_spin_10_parity_project_trace, mul_spin_11_parity_project_trace,
     mul_spin_12_parity_project_trace, mul_spin_13_parity_project_trace},
    {mul_spin_20_parity_project_trace, mul_spin_21_parity_project_trace,
     mul_spin_22_parity_project_trace, mul_spin_23_parity_project_trace},
    {mul_spin_30_parity_project_trace, mul_spin_31_parity_project_trace,
     mul_spin_32_parity_project_trace, mul_spin_33_parity_project_trace}};

prop_matrix fetch_prop_matrix(spinor_dble **prop, int ipt)
{
  int sci;
  prop_matrix result;

  for (sci = 0; sci < 12; ++sci) {
    memcpy(result.p[sci], prop[sci] + ipt, sizeof(spinor_dble));
  }

  return result;
}

/* Makes a 4x4 spin matrix using colour indices ia and ib */
spin_matrix fetch_spin_matrix(prop_matrix *prop, int ia, int ib)
{
  int i, j;
  spin_matrix result;

  for (i = 0; i < 4; ++i) {
    for (j = 0; j < 4; ++j) {
      result.s[j][i] = (*prop).p[3 * i + ia][3 * j + ib];
    }
  }

  return result;
}
