/*******************************************************************************
 *
 * File gamma_prod.c
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * The externally accessible functions are
 *   void g5_gamma_dag_prod(spinor_dble *s, int gidx)
 *     Replace s by (g5 g(gidx)^{dag} s)
 *
 *   void gamma_g5_prod(spinor_dble *s, int gidx)
 *     Replace s by (g(gidx) g5 s)
 *
 * Notes:
 *
 *   The g(i) are the 16 gamma matrices. The definition and order of these is
 *   outlined in the documentation for the openqcd-mu-squared project.
 *
 *******************************************************************************/

#define GAMMA_PROD_C
#define OPENQCD_INTERNAL

#ifdef __cplusplus
extern "C" {
#endif

#include "gamma_prod.h"

#include "openqcd/c_headers/linalg.h"
#include "openqcd/c_headers/sflds.h"
#include "openqcd/c_headers/utils.h"

#ifdef __cplusplus
}
#endif

int init = 0;
static complex_dble g5_gammadag[16][4][4];
static complex_dble gamma_g5[16][4][4];

typedef union
{
  spinor_dble s;
  complex_dble c[12];
} spin_t;

/* Initialise the 16 g5_gammadag matrices, and then 16 gamma_g5 matrices */
static void init_gamma_matrices(void)
{
  int ig, ia, ib;

  for (ig = 0; ig < 16; ++ig) {
    for (ia = 0; ia < 4; ++ia) {
      for (ib = 0; ib < 4; ++ib) {
        g5_gammadag[ig][ia][ib].re = 0.0;
        g5_gammadag[ig][ia][ib].im = 0.0;

        gamma_g5[ig][ia][ib].re = 0.0;
        gamma_g5[ig][ia][ib].im = 0.0;
      }
    }
  }

  /* gamma[0] = Identity */

  g5_gammadag[0][0][0].re = 1.0;
  g5_gammadag[0][1][1].re = 1.0;
  g5_gammadag[0][2][2].re = -1.0;
  g5_gammadag[0][3][3].re = -1.0;

  gamma_g5[0][0][0].re = 1.0;
  gamma_g5[0][1][1].re = 1.0;
  gamma_g5[0][2][2].re = -1.0;
  gamma_g5[0][3][3].re = -1.0;

  /* gamma[1] = g0 (gamma time) */

  g5_gammadag[1][0][2].re = -1.0;
  g5_gammadag[1][1][3].re = -1.0;
  g5_gammadag[1][2][0].re = 1.0;
  g5_gammadag[1][3][1].re = 1.0;

  /* gamma[2] = g1 (gamma x) */

  g5_gammadag[2][0][3].im = -1.0;
  g5_gammadag[2][1][2].im = -1.0;
  g5_gammadag[2][2][1].im = -1.0;
  g5_gammadag[2][3][0].im = -1.0;

  /* gamma[3] = g2 (gamma y) */

  g5_gammadag[3][0][3].re = -1.0;
  g5_gammadag[3][1][2].re = 1.0;
  g5_gammadag[3][2][1].re = -1.0;
  g5_gammadag[3][3][0].re = 1.0;

  /* gamma[4] = g3 (gamma z) */

  g5_gammadag[4][0][2].im = -1.0;
  g5_gammadag[4][1][3].im = 1.0;
  g5_gammadag[4][2][0].im = -1.0;
  g5_gammadag[4][3][1].im = 1.0;

  /* gamma[5] = g5 */

  g5_gammadag[5][0][0].re = 1.0;
  g5_gammadag[5][1][1].re = 1.0;
  g5_gammadag[5][2][2].re = 1.0;
  g5_gammadag[5][3][3].re = 1.0;

  /* gamma[6] = g5g1 */

  g5_gammadag[6][0][2].re = 1.0;
  g5_gammadag[6][1][3].re = 1.0;
  g5_gammadag[6][2][0].re = 1.0;
  g5_gammadag[6][3][1].re = 1.0;

  /* gamma[7] = g5g2 */

  g5_gammadag[7][0][3].im = 1.0;
  g5_gammadag[7][1][2].im = 1.0;
  g5_gammadag[7][2][1].im = -1.0;
  g5_gammadag[7][3][0].im = -1.0;

  /* gamma[8] = g5g3 */

  g5_gammadag[8][0][3].re = 1.0;
  g5_gammadag[8][1][2].re = -1.0;
  g5_gammadag[8][2][1].re = -1.0;
  g5_gammadag[8][3][0].re = 1.0;

  /* gamma[9] = g5g4 */

  g5_gammadag[9][0][2].im = 1.0;
  g5_gammadag[9][1][3].im = -1.0;
  g5_gammadag[9][2][0].im = -1.0;
  g5_gammadag[9][3][1].im = 1.0;

  /* gamma[10] = sig(0,1) */

  g5_gammadag[10][0][1].re = 1.0;
  g5_gammadag[10][1][0].re = 1.0;
  g5_gammadag[10][2][3].re = 1.0;
  g5_gammadag[10][3][2].re = 1.0;

  /* gamma[11] = sig(0,2) */

  g5_gammadag[11][0][1].im = -1.0;
  g5_gammadag[11][1][0].im = 1.0;
  g5_gammadag[11][2][3].im = -1.0;
  g5_gammadag[11][3][2].im = 1.0;

  /* gamma[12] = sig(0,3) */

  g5_gammadag[12][0][0].re = 1.0;
  g5_gammadag[12][1][1].re = -1.0;
  g5_gammadag[12][2][2].re = 1.0;
  g5_gammadag[12][3][3].re = -1.0;

  /* gamma[13] = sig(1,2) */

  g5_gammadag[13][0][0].re = -1.0;
  g5_gammadag[13][1][1].re = 1.0;
  g5_gammadag[13][2][2].re = 1.0;
  g5_gammadag[13][3][3].re = -1.0;

  /* gamma[14] = sig(1,3) */

  g5_gammadag[14][0][1].im = -1.0;
  g5_gammadag[14][1][0].im = 1.0;
  g5_gammadag[14][2][3].im = 1.0;
  g5_gammadag[14][3][2].im = -1.0;

  /* gamma[15] = sig(2,3) */

  g5_gammadag[15][0][1].re = -1.0;
  g5_gammadag[15][1][0].re = -1.0;
  g5_gammadag[15][2][3].re = 1.0;
  g5_gammadag[15][3][2].re = 1.0;

  /* Fill the g[i].g5 matrices based on symmetry */

  for (ig = 1; ig < 5; ++ig) {
    for (ia = 0; ia < 4; ++ia) {
      for (ib = 0; ib < 4; ++ib) {
        gamma_g5[ig][ia][ib].re = -g5_gammadag[ig][ia][ib].re;
        gamma_g5[ig][ia][ib].im = -g5_gammadag[ig][ia][ib].im;
      }
    }
  }

  for (ig = 5; ig < 16; ++ig) {
    for (ia = 0; ia < 4; ++ia) {
      for (ib = 0; ib < 4; ++ib) {
        gamma_g5[ig][ia][ib] = g5_gammadag[ig][ia][ib];
      }
    }
  }

  init = 1;
}

/* Replace s by (g5 g(gidx)^dag s */
static void mult_g5_gamma_dag(spinor_dble *s, int gidx)
{
  int is1, is2, ic;
  spin_t buffer;
  double *dptr;

  buffer.s = *s;
  dptr = (double *)s;

  for (ic = 0; ic < 3; ++ic) {
    for (is1 = 0; is1 < 4; ++is1) {
      dptr[2 * (ic + 3 * is1)] = 0.0;
      dptr[2 * (ic + 3 * is1) + 1] = 0.0;
      for (is2 = 0; is2 < 4; ++is2) {
        dptr[2 * (ic + 3 * is1)] +=
            g5_gammadag[gidx][is1][is2].re * buffer.c[ic + 3 * is2].re -
            g5_gammadag[gidx][is1][is2].im * buffer.c[ic + 3 * is2].im;
        dptr[2 * (ic + 3 * is1) + 1] +=
            g5_gammadag[gidx][is1][is2].re * buffer.c[ic + 3 * is2].im +
            g5_gammadag[gidx][is1][is2].im * buffer.c[ic + 3 * is2].re;
      }
    }
  }
}

/* Replace s by (g5 g(gidx)^dag s */
static void mult_gamma_g5(spinor_dble *s, int gidx)
{
  int is1, is2, ic;
  spin_t buffer;
  double *dptr;

  buffer.s = *s;
  dptr = (double *)s;

  for (ic = 0; ic < 3; ++ic) {
    for (is1 = 0; is1 < 4; ++is1) {
      dptr[2 * (ic + 3 * is1)] = 0.0;
      dptr[2 * (ic + 3 * is1) + 1] = 0.0;
      for (is2 = 0; is2 < 4; ++is2) {
        dptr[2 * (ic + 3 * is1)] +=
            g5_gammadag[gidx][is1][is2].re * buffer.c[ic + 3 * is2].re -
            g5_gammadag[gidx][is1][is2].im * buffer.c[ic + 3 * is2].im;
        dptr[2 * (ic + 3 * is1) + 1] +=
            g5_gammadag[gidx][is1][is2].re * buffer.c[ic + 3 * is2].im +
            g5_gammadag[gidx][is1][is2].im * buffer.c[ic + 3 * is2].re;
      }
    }
  }
}

void g5_gamma_dag_prod(spinor_dble *s, int gidx)
{
  if (init == 0) {
    init_gamma_matrices();
  }

  switch (gidx) {
  case 0:
    mulg5_dble(1, s);
    break;
  case 5:
    return;
  default:
    mult_g5_gamma_dag(s, gidx);
  }
}

void gamma_g5_prod(spinor_dble *s, int gidx)
{
  if (init == 0) {
    init_gamma_matrices();
  }

  switch (gidx) {
  case 0:
    mulg5_dble(1, s);
    break;
  case 5:
    return;
  default:
    mult_gamma_g5(s, gidx);
  }
}
