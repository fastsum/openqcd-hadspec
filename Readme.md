# openQCD Hadron Spectrum Computation Program

openqcd-hadspec provides functionality for computing the hadron spectrum using
existing gauge configurations. The program currently computes the following
particles:

 * 2 flavour meson
 * 2 flavour J=1/2 baryons, positive parity projection
 * 1 flavour J=3/2 baryons, positive parity projection
 * 2 flavour J=3/2 baryons, positive parity projection

The sources are set in the same way as in openqcd-propagator, and we therefore
support two different source types:

 * Point sources
 * Gaussian smeared sources

The code builds on top of openqcd-fastsum, and one can therefore use the Dirac
operator as provided by that code:

 * Order a improved Wilson fermions
 * Optional anisotropic actions
 * Stout smeared gauge links

## Building

To compile the project simply `cd` into the `build` directory and run `make`.
See the Readme file inside of the `build` directory for more details.

### Requirements

 * v1.1 of [openqcd-fastsum][openqcd-fastsum lib]
 * v1.0 of [openqcd-propagator][openqcd-propagator lib]
 * an implementation of MPI

## Usage

The main output of the program is the `openqcd-hadspec` executable which runs
through pre-existing lattice gauge configurations and computes hadronic
correlation functions with parameters as set by the input file. The program has
the following usage:

```
usage: openqcd-hadspec -i <input file> [-a] [-seed <seed>]

Program Parameters:
-i <input file>  Name of the input file from which the simulation parameters are
                 read

Optional parameters:
-a               Run in append mode to continue a previously started run from
                 the last successful computation
-seed <seed>     Set the simulation RNG seed, overriding the infile seed and old
                 seeds in continuation runs
```

The program infile is has many of the same sections as openqcd, and an example
of a complete infile can be found at `main/examples/hadspec.in`. The input
parameters are similar to those in openqcd-propagator with the addition of the
following hadspec section

```
[Hadspec]
quarks 0 0 1 2
names u d s c

[Quark 0]
im0    0
isolv  0
smear  1

[Quark 1]
im0    1
isolv  0
smear  1

[Quark 2]
im0    2
isolv  0
smear  1
ani    1.078 4.3 1.3545694350 0.7939900651 
```

This section tells the program that we have 4 quark flavours with labels u, d,
s, and c. In this case the u and d are degenerate something the program takes
into account when constructing valid particles. In the case of the charm quark
we have supplied a different set of anisotropy parameters.

It also supports the following definition of the deflation subspace parameters:

```
[Deflation subspace]
bs      4 4 4 4
Ns      28
retries 100
```

where `retries` tells the program how many times to attempt to make a deflation
subspace before it gives up. This is necessary due to a [bug][deflation bug] in
openqcd-fastsum, and should not be necessary once this has been fixed.


## Authors

This code was written by Jonas Rylund Glesaaen while a post-doc in the lattice
group at Swansea University.

## License

The software may be used under the terms of the GNU General Public Licence
(GPL).

[openqcd-fastsum lib]: https://gitlab.com/fastsum/openqcd-fastsum
[openqcd-propagator lib]: https://gitlab.com/fastsum/openqcd-propagator
[deflation bug]: https://gitlab.com/fastsum/openqcd-fastsum/issues/81
