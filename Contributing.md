# How to contribute

Contributions to the code are most welcome, but for contributions to be accepted
they most conform with the standards of the repository. The two most important
factors are:

## Code formatting and style

The code is auto-formatted using `clang-format-6.0` using the file
[`.clang-format`](.clang-format) file available in the repository. Code that
doesn't comply with this will not be accepted. Other than that, please adhere
to the general programming style such as using the comment header on source and
header files, and adding comments to static functions.

A script is provided for running clang-format on the files in the repository,
simply run

```bash
./scripts/format_code.sh
```

## Testing

Any new code submitted should have tests written for them, and any changed code
should have their tests updated. The unit test suite is expected to run both in
serial and in parallel, so make sure your tests handles these two cases as
expected.

As is usual when testing C-code, most of the tests includes the source file it
tests so that it can also access static methods. For the build process to
recognise this and not compile the source twice, the test file must have the
same name as the tested source, prefixed by `test_` and with a `.cpp` file type
instead of `.c`. If one does not wish for this to happen, make sure that the
test source file does not have the same name as a code source.

## Submitting issues

Issues can be submitted to the [project issue tracker][issue tracker].

## Submitting changes

Please submit changes as a [GitLab merge request][merge request] with a clear
indication of what has been changed, linking to any issues where appropriate.
Please submit the pull request toward the devel branch as the master branch is
reserved for releases. Only fast-forward merge requests will be accepted, so you
have to rebase off the current devel for the merge request to be accepted.

[issue tracker]: https://gitlab.com/fastsum/openqcd-hadspec/issues
[merge request]: https://gitlab.com/fastsum/openqcd-hadspec/merge_requests/new
