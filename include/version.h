/*******************************************************************************
 *
 * File version.h
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#ifndef OPENQCD_MU_SQR__VERSION_H
#define OPENQCD_MU_SQR__VERSION_H

#define openqcd_hadspec__RELEASE "openqcd-hadspec v0.1"

extern const char *openqcd_hadspec__build_date;
extern const char *openqcd_hadspec__build_git_sha;
extern const char *openqcd_hadspec__build_user_cflags;

#endif /* OPENQCD__VERSION_H */
