/*******************************************************************************
 *
 * File gamma_prod.h
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#ifndef OPENQCD__GAMMA_PROD_H
#define OPENQCD__GAMMA_PROD_H

#include "openqcd/c_headers/su3.h"

/* GAMMA_PROD_C */
extern void openqcd_mu_squared__g5_gamma_dag_prod(openqcd__spinor_dble *s,
                                                  int gidx);
extern void openqcd_mu_squared__gamma_g5_prod(openqcd__spinor_dble *s,
                                              int gidx);

#if defined(OPENQCD_INTERNAL)

/* GAMMA_PROD_C */
#define g5_gamma_dag_prod(...)                                                 \
  openqcd_mu_squared__g5_gamma_dag_prod(__VA_ARGS__)
#define gamma_g5_prod(...) openqcd_mu_squared__gamma_g5_prod(__VA_ARGS__)

#endif /* defined OPENQCD_INTERNAL */

#endif /* OPENQCD__GAMMA_PROD_H */
