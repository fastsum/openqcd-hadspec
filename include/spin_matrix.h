/*******************************************************************************
 *
 * File spin_matrix.h
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#ifndef OPENQCD__SPIN_MATRIX_H
#define OPENQCD__SPIN_MATRIX_H

#include "openqcd/c_headers/su3.h"

typedef struct
{
  openqcd__complex_dble p[12][12] openqcd__ALIGNED32;
} prop_matrix;

typedef struct
{
  openqcd__complex_dble s[4][4] openqcd__ALIGNED32;
} spin_matrix;

extern spin_matrix transpose_spin_matrix(spin_matrix const *sm);
extern openqcd__complex_dble trace_spin_matrix(spin_matrix const *sm);
extern spin_matrix mulc_spin_matrix(openqcd__complex_dble c,
                                    spin_matrix const *sm);
extern spin_matrix mul_spin_spin_matrix(spin_matrix const *smA,
                                        spin_matrix const *smB);
extern spin_matrix mul_spin_spin_matrixransposed_matrix(spin_matrix const *smA,
                                                        spin_matrix const *smB);
extern spin_matrix mul_charge_gamma(spin_matrix const *sm, int g);
extern openqcd__complex_dble mul_parity_project_trace(spin_matrix const *sm,
                                                      int p);
extern openqcd__complex_dble (*mul_spin_parity_project_trace[4][4])(
    spin_matrix const *sm);
prop_matrix fetch_prop_matrix(openqcd__spinor_dble **prop, int ipt);
spin_matrix fetch_spin_matrix(prop_matrix *prop, int ia, int ib);

#endif /* OPENQCD__SPIN_MATRIX_H */
