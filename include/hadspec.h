/*******************************************************************************
 *
 * File hadspec.h
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#ifndef OPENQCD_HADSPEC__HADSPEC_H
#define OPENQCD_HADSPEC__HADSPEC_H

#include "openqcd-propagator/c_headers/run_timer.h"
#include "openqcd/c_headers/su3.h"
#include <stdlib.h>

typedef struct
{
  int im0;
  int smear;
  int isolv;
  double ani[4];
} quark_parms_t;

#define MAX_QUARK_CONSTITUENTS 32

/* HADSPEC_BARYONS_C */
extern size_t doublet_size(void);
extern size_t quadruplet_size(void);
extern void doublet_2fl_baryon(int iquark1, int iquark2,
                               openqcd__complex_dble *out);
extern void quadruplet_1fl_baryon(int iquark, openqcd__complex_dble *out);
extern void quadruplet_2fl_baryon(int iquark1, int iquark2,
                                  openqcd__complex_dble *out);

/* HADSPEC_COMBINATIONS_C */
extern void init_hadspec_combinations(int *quark_list, char *quark_names,
                                      int num);
extern int *baryon_1flavour_list(int *num);
extern int **baryon_2flavour_list(int *num);
extern int **meson_list(int *num);
extern char *baryon_1flavour_names(int *num);
extern char **baryon_2flavour_names(int *num);
extern char **meson_names(int *num);

/* HADSPEC_MESONS_C */
size_t meson_size(void);
void meson(int iquark1, int iquark2, openqcd__complex_dble *out);

/* HADSPEC_OUTPUT_C */
void print_doublet(char const *filename, openqcd__complex_dble const *doublet,
                   int isrc);
void print_quadruplet(char const *filename,
                      openqcd__complex_dble const *quadruplet, int isrc);
void print_meson(char const *filename, openqcd__complex_dble const *meson,
                 int isrc);

/* HADSPEC_PARAMS_C */
extern void reset_quark_parms(void);
extern void set_quark_parms(int iquark, int im0, int smear, int isolv,
                            double nu, double xi, double cR, double cT);
extern void read_quark_parms(int iquark);
extern quark_parms_t quark_parms(int iquark);

/* HADSPEC_PROPAGATORS_C */
extern void init_propagators(int *quark_ids, int num);
extern openqcd__spinor_dble **quark_propagators(int iquark);
extern openqcd_run_timer__timing_info_t
compute_quark_propagator(int isrc, int iquark, int *status);

#if defined(OPENQCD_INTERNAL)

#endif /* defined OPENQCD_INTERNAL */

#endif /* OPENQCD_HADSPEC__HADSPEC_H */
